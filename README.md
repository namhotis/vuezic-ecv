# Vuezic ECV

Music news website using VueJS & NodeJS   
Mathis DYK & Isabelle REGNIER 

---

**Front technologies:** VueJS, VueX, ElementUI  
**Back technologies:** NodeJS, Express, Mongoose  
**Auth:** PassportJS  
**Storage:** MongoDB  

## Project setup

> - If you're running on a Linux-based system, please check if the mongod service is started and active
>
> - Run the following commands from `vuezic-ecv` root directory

### Install dependencies

```
cd server/
npm install
```

```
cd client/
npm install
``` 

### Populate the MongoDB database
```
mongorestore dump
```

### Start the app

```
cd server/
npm run start

```

```
cd client/
npm run serve

```

The front app will be running at http://localhost:8080/

## Use the app

Create an admin account to access Create, Read, Update & Delete methods  
Create an user account to access Read method

## Features

* Admin & User registration
* Artist, Album, Concert & News CRUD
* Role based routing
* MongoDB storage
* Passport authentication
* ElementUI implementation
* Search bar for artists & albums
* Error handling & UI notifications