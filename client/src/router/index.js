import Vue from 'vue'
import VueRouter from 'vue-router'
import Artists from '@/views/Artists.vue'
import Artist from '@/views/Artist.vue'
import Concerts from '@/views/Concerts.vue'
import NewsArticle from '@/views/NewsArticle.vue'
import NewsArticles from '@/views/NewsArticles.vue'
import Albums from '@/views/Albums.vue'

// Create
import ArtistCreate from '@/views/create/ArtistCreate.vue'
import AlbumCreate from '@/views/create/AlbumCreate.vue'
import ConcertCreate from '@/views/create/ConcertCreate.vue'
import NewsArticleCreate from '@/views/create/NewsArticleCreate.vue'

// Edit
import ArtistEdit from '@/views/edit/ArtistEdit.vue'
import AlbumEdit from '@/views/edit/AlbumEdit.vue'
import NewsEdit from '@/views/edit/NewsEdit.vue'
import ConcertEdit from '@/views/edit/ConcertEdit.vue'

// Auth
import Signup from '@/views/Signup.vue'
import Login from '@/views/Login.vue'
import Home from '@/views/Home.vue'
import store from '../store'

Vue.use(VueRouter)

// Contient l'ensemble des routes ([]) dans une variable
// [] contient {} avec 3 propriétés : (TODO: checker s'il existe des non obligatoires)
// {
//   path: String : où l'utilisateur est actuellement
//   name: String : ??? (TODO : Aller voir ce que signifie la propriété name)
//   component: Le component à Afficher
// }
const routes = [
  // CASU
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/artists',
    name: 'Artists',
    component: Artists
  },
  {
    path: '/artist/:id',
    name: 'Artist',
    component: Artist
  },
  {
    path: '/concerts',
    name: 'Concerts',
    component: Concerts
  },
  {
    path: '/albums',
    name: 'Albums',
    component: Albums
  },
  {
    path: '/news',
    name: 'news',
    component: NewsArticles
  },
  {
    path: '/news/:id',
    name: 'new',
    component: NewsArticle
  },

  // ADMIN
  // Create
  {
    path: '/create/artist',
    name: 'ArtistCreate',
    component: ArtistCreate,
    meta: {
      requiresAuth: true,
      requiredAdmin: true
    }
  },
  {
    path: '/create/album',
    name: 'AlbumCreate',
    component: AlbumCreate,
    meta: {
      requiresAuth: true,
      requiredAdmin: true
    }
  },
  {
    path: '/create/concert',
    name: 'ConcertCreate',
    component: ConcertCreate,
    meta: {
      requiresAuth: true,
      requiredAdmin: true
    }
  },
  {
    path: '/create/newsArticle',
    name: 'NewsArticleCreate',
    component: NewsArticleCreate,
    meta: {
      requiresAuth: true,
      requiredAdmin: true
    }
  },

  // Edit
  {
    path: '/edit/artist/:id',
    name: 'ArtistEdit',
    component: ArtistEdit,
    meta: {
      requiresAuth: true,
      requiredAdmin: true
    }
  },
  {
    path: '/edit/album/:id',
    name: 'AlbumEdit',
    component: AlbumEdit,
    meta: {
      requiresAuth: true,
      requiredAdmin: true
    }
  },
  {
    path: '/edit/news/:id',
    name: 'NewsEdit',
    component: NewsEdit,
    meta: {
      requiresAuth: true,
      requiredAdmin: true
    }
  },
  {
    path: '/edit/concert/:id',
    name: 'ConcertEdit',
    component: ConcertEdit,
    meta: {
      requiresAuth: true,
      requiredAdmin: true
    }
  },

  // ANON
  {
    path: '/signup',
    name: 'Signup',
    component: Signup,
    meta: { guest: true }
  },
  {
    path: '/login',
    name: 'login',
    component: Login,
    meta: { guest: true }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

// TODO : Vérifier : Un seul if par beforeEach ?
router.beforeEach((to, from, next) => {
  // TO : Route on l'on souhaite aller
  // FROM : route d'où l'on vient
  // NEXT : semble être la fonction utlisée pour résoudre le hooks.
  // En gros la fonction de qui dirige l'auteur

  // ROUTE ADMIN
  // Si la route a la propriété requiresAuth ET requiredAdmin
  if (to.matched.some(
    record => record.meta.requiresAuth && record.meta.requiredAdmin
  )) {
    // Si user est admin + auth, on accepte l'accès à la page
    if (store.getters.isAuthenticated && store.getters.isAdmin) {
      next()
      return
    }
    // Sinon on l'invite à se login.
    next('/login')
  } else {
    next()
  }
})

router.beforeEach((to, from, next) => {
  // ROUTE ANON
  // Si la route a la propriété guest
  if (to.matched.some(record => record.meta.guest)) {
    // Si personne n'ext connecté, on lui laisse accès
    if (store.getters.isAuthenticated === false) {
      next()
      return
    }
    // Sinon on le redirige vers la home
    next('/')
  } else {
    next()
  }
})

export default router
