import Vuex from 'vuex'
import Vue from 'vue'
import createPersistedState from 'vuex-persistedstate'
import auth from './modules/auth'
import artists from './modules/artists'
import albums from './modules/albums'
import concerts from './modules/concerts'
import news from './modules/newsArticles'

// Load Vuex
Vue.use(Vuex)

// Create store
export default new Vuex.Store({
  modules: {
    auth,
    artists,
    albums,
    concerts,
    news
  },
  plugins: [createPersistedState()]
})
