import axios from 'axios'

const state = {
  newsArticles: [],
  newsArticle: null
}

const getters = {
  getNewsArticles: state => state.newsArticles,
  getNewsArticle: state => state.newsArticle,
  deleteNewsArticle: state => state.article
}

const actions = {
  async fetchNewsArticles ({ commit }) {
    const { data } = await axios.get(`${process.env.VUE_APP_API_URL}newsArticles`)
    commit('setNewsArticles', data)
  },

  async fetchNewsArticle ({ commit }, id) {
    const { data } = await axios.get(
      `${process.env.VUE_APP_API_URL}newsArticles/${id}`
    )
    commit('setNewsArticle', data)
  },

  async deleteNewsArticle ({ commit }, id) {
    await axios.delete(`${process.env.VUE_APP_API_URL}newsArticles/${id}`)
    commit('deleteNewsArticle')
  }
}

const mutations = {
  setNewsArticles (state, newsArticles) {
    state.all = newsArticles
  },
  setNewsArticle (state, newsArticle) {
    state.newsArticle = newsArticle
  },
  deleteNewsArticle (state) {
    state.newsArticle = null
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
