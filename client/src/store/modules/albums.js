import axios from 'axios'
const state = {
  albums: [],
  artist: null
}

const getters = {
  getAlbums: state => state.albums,
  getAlbum: state => state.album,
  deleteAlbum: state => state.album
}

const actions = {
  async fetchAlbums ({ commit }) {
    const { data } = await axios.get(`${process.env.VUE_APP_API_URL}albums`)
    commit('setAlbums', data)
  },

  async fetchAlbum ({ commit }, id) {
    const { data } = await axios.get(`${process.env.VUE_APP_API_URL}albums/${id}`)
    commit('setAlbum', data)
  },

  async deleteAlbum ({ commit }, id) {
    await axios.delete(`${process.env.VUE_APP_API_URL}albums/${id}`)
    commit('deleteAlbum')
  }
}

const mutations = {
  setAlbums (state, albums) {
    state.all = albums
  },
  setAlbum (state, album) {
    state.album = album
  },
  deleteAlbum (state) {
    state.album = null
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
