import axios from 'axios'

// Etat par défaut du store
const state = {
  user: null,
  isAdmin: null
}

const getters = {
  isAuthenticated: state => !!state.user,
  StateUser: state => state.user,
  isAdmin: state => state.isAdmin
}

// TODO : déplacer les actions dans un autre fichier.
const actions = {
  async Register ({
    dispatch
  }, form) {
    await axios.post(`${process.env.VUE_APP_API_URL}signup`, form)
    const UserForm = {
      email: form.email,
      password: form.password,
      isAdmin: form.role
    }
    await dispatch('LogIn', UserForm)
  },

  async LogIn ({
    commit
  }, User) {
    await axios.post(`${process.env.VUE_APP_API_URL}login`, User, {
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then((response) => {
        commit('setIsAdmin', response.data.isAdmin)
      })
    const user = JSON.parse(User)
    await commit('setUser', user)
  },

  async LogOut ({
    commit
  }) {
    // On appelle les mutations
    // On indique que l'utilisateur n'ext plus admin ni user
    commit('setIsAdmin', 0)
    commit('setUser', 0)
  }
}

const mutations = {
  // Une seule propriété de state par set
  setUser (state, user) {
    state.user = user
  },
  setIsAdmin (state, isAdmin) {
    state.isAdmin = isAdmin
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
