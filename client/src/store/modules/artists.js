import axios from 'axios'
const state = {
  artist: null
}

const getters = {
  getArtist: state => state.artist,
  deleteArtist: state => state.artist
}

const actions = {
  async fetchArtists ({ commit }) {
    const { data } = await axios.get(`${process.env.VUE_APP_API_URL}artists`)
    commit('setArtists', data)
  },

  async fetchArtist ({ commit }, id) {
    const { data } = await axios.get(`${process.env.VUE_APP_API_URL}artists/${id}`)
    commit('setArtist', data)
  },

  async deleteArtist ({ commit }, id) {
    await axios.delete(`${process.env.VUE_APP_API_URL}artists/${id}`)
    commit('deleteArtist')
  }
}

const mutations = {
  setArtists (state, artists) {
    state.all = artists
  },
  setArtist (state, artist) {
    state.artist = artist
  },
  deleteArtist (state) {
    state.artist = null
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
