import axios from 'axios'

const state = {
  concerts: [],
  concert: null
}

const getters = {
  getConcerts: state => state.concerts,
  getConcert: state => state.concert,
  deleteConcert: state => state.concert
}

const actions = {
  async fetchConcerts ({ commit }) {
    const { data } = await axios.get(`${process.env.VUE_APP_API_URL}concerts`)
    commit('setConcerts', data)
  },

  async fetchConcert ({ commit }, id) {
    const { data } = await axios.get(
      `${process.env.VUE_APP_API_URL}concerts/${id}`
    )
    commit('setConcert', data)
  },

  async deleteConcert ({ commit }, id) {
    await axios.delete(`${process.env.VUE_APP_API_URL}concerts/${id}`)
    commit('deleteConcert')
  }
}

const mutations = {
  setConcerts (state, concerts) {
    state.all = concerts
  },
  setConcert (state, concert) {
    state.concert = concert
  },
  deleteConcert (state) {
    state.concert = null
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
