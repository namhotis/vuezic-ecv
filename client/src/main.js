import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import axios from 'axios'
import locale from 'element-ui/lib/locale/lang/en'
import 'fontsource-roboto'

Vue.use(ElementUI, { locale })

axios.interceptors.response.use(undefined, function (error) {
  if (error) {
    const originalRequest = error.config
    if (error.response.status === 401 && !originalRequest._retry) {
      originalRequest._retry = true
      store.dispatch('LogOut')
      return router.push('/login')
    }
  }
})

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
