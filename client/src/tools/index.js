import { validURL, readURL } from './url'

const tools = {
  validURL: url => validURL(url),
  readURL: e => readURL(e)
}

export default tools
