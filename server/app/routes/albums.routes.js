import uploadDisk from '../../storage';

const album = require('../controllers/albums.controller.js');

module.exports = (app) => {
  // Create a new Album
  app.post('/albums', uploadDisk.single('cover'), album.create);
  app.get('/albums/:artistId', album.artistByAlbum);

  // Retrieve all album
  app.get('/albums', album.findAll);

  // Retrieve a single Album with albumId
  app.get('/albums/:albumId', album.findOne);

  // Update a Album with albumId
  app.put('/albums/:albumId', uploadDisk.single('cover'), album.update);

  // Delete a Album with albumId
  app.delete('/albums/:albumId', album.delete);
};
