import uploadDisk from '../../storage';

const newsArticle = require('../controllers/newsArticles.controller.js');

module.exports = (app) => {
  // Retrieve all new
  app.post('/newsArticles', uploadDisk.single('cover'), newsArticle.create);
  app.get('/newsArticles', newsArticle.findAll);

  // Retrieve a single Artist with newId
  app.get('/newsArticles/:newsArticleId', newsArticle.findOne);

  // Update a Artist with newId
  app.put('/newsArticles/:newsArticleId', uploadDisk.single('cover'), newsArticle.update);

  // Delete a Artist with newId
  app.delete('/newsArticles/:newsArticleId', newsArticle.delete);
};
