const concert = require('../controllers/concerts.controller.js');

module.exports = (app) => {
  // Create a new Concert
  app.post('/concerts', concert.create);
  app.get('/concerts/:artistId', concert.artistByConcert);

  // Retrieve all concert
  app.get('/concerts', concert.findAll);

  // Retrieve a single Concert with concertId
  app.get('/concerts/:concertId', concert.findOne);

  // Update a Concert with concertId
  app.put('/concerts/:concertId', concert.update);

  // Delete a Concert with concertId
  app.delete('/concerts/:concertId', concert.delete);
};
