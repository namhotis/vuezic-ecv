import uploadDisk from '../../storage';

const artist = require('../controllers/artists.controller.js');

module.exports = (app) => {
  // Retrieve all artist
  app.post('/artists', uploadDisk.single('image'), artist.create);
  app.get('/artists', artist.findAll);

  // Retrieve a single Artist with artistId
  app.get('/artists/:artistId', artist.findOne);

  // Update a Artist with artistId
  app.put('/artists/:artistId', uploadDisk.single('image'), artist.update);

  // Delete a Artist with artistId
  app.delete('/artists/:artistId', artist.delete);
};
