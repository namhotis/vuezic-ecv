const Album = require('../models/album.model');
const Artist = require('../models/artist.model');

module.exports = {
  create: (req, res) => {
    if (!req.body.name) {
      return res.status(400).send({
        message: 'Album name can not be empty',
      });
    }

    const newAlbum = new Album({
      artist: req.body.album_artist,
      name: req.body.name,
      release_date: req.body.release_date,
      cover: process.env.UPLOAD_DOMAIN + req.file.path,
    });

    newAlbum
      .save((error, album) => {
        if (!error) {
          Artist.findOne({
            _id: album.artist,
          }).exec((_error, artist) => {
            if (!artist.albums) {
              artist.albums = [];
            }

            artist.albums.push(album);
            artist.save();
          });

          Album.findOne({
            _id: album._id,
          })
            .populate('artist')
            .exec((_error, _album) => {
              if (_error) res.send(_error);
              if (!error && _album) res.status(200).send(_album);
            });
        }
        console.log(error, album);
      });
  },
  artistByAlbum: async (req, res) => {
    const {
      artistId,
    } = req.params;
    const artistByAlbum = await Album.findById(artistId).populate('artist');
    res.send(artistByAlbum);
  },

  albumsByArtist: (req, res) => {
    const {
      artistId,
    } = req.params;
    Artist.findById(artistId).populate('albums')
      .then((albums) => {
        res.send(albums);
      })
      .catch((err) => {
        res.status(500).send({
          message: err.message || 'Some error occurred while retrieving albums.',
        });
      });
  },

  findAll: (req, res) => {
    Album.find({})
      .populate('artist')
      .exec((err, albums) => {
        if (err) return res.send(err);
        res.send(
          albums.map((album) => {
            const tmpAlbum = {
              name: album.name,
              _id: album._id,
              cover: album.cover,
              artist: null,
              release_date: album.release_date,
            };

            if (album.artist) {
              tmpAlbum.artist = {
                name: album.artist.name,
                profile_picture: album.artist.profile_picture,
                _id: album.artist._id,
              };
            }

            return tmpAlbum;
          }),
        );
      });
  },

  findOne: (req, res) => {
    Album.findById(req.params.albumId)
      .then((album) => {
        if (!album) {
          return res.status(404).send({
            message: `Album not found with id ${req.params.albumId}`,
          });
        }

        album.artist = {
          name: album.artist.name,
          profile_picture: album.artist.name,
        };

        res.send(album);
      }).catch((err) => {
        if (err.kind === 'ObjectId') {
          return res.status(404).send({
            message: `Album not found with id ${req.params.albumId}`,
          });
        }
        return res.status(500).send({
          message: `Error retrieving album with id ${req.params.albumId}`,
        });
      });
  },

  update: (req, res) => {
    const handledReqBody = req.body;

    if (req.file) {
      handledReqBody.cover = process.env.UPLOAD_DOMAIN + req.file.path;
    }

    Album.findByIdAndUpdate(req.params.albumId, handledReqBody, {
      new: true,
    })
      .then((album) => {
        if (!album) {
          return res.status(404).send({
            message: `Album not found with id ${req.params.albumId}`,
          });
        }
        res.send(album);
      })
      .catch((err) => {
        if (err.kind === 'ObjectId') {
          return res.status(404).send({
            message: `Album not found with id ${req.params.albumId}`,
          });
        }
        return res.status(500).send({
          message: `Error updating album with id ${req.params.albumId}`,
        });
      });
  },

  delete: (req, res) => {
    Album.findByIdAndRemove(req.params.albumId)
      .then((album) => {
        if (!album) {
          return res.status(404).send({
            message: `Album not found with id ${req.params.albumId}`,
          });
        }
        res.send({
          message: 'Album deleted successfully!',
        });
      }).catch((err) => {
        if (err.kind === 'ObjectId' || err.name === 'NotFound') {
          return res.status(404).send({
            message: `Album not found with id ${req.params.albumId}`,
          });
        }
        return res.status(500).send({
          message: `Could not delete album with id ${req.params.albumId}`,
        });
      });
  },
};
