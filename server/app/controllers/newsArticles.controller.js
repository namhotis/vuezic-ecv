const NewsArticles = require('../models/newsArticle.model.js');

module.exports = {
  create: (req, res) => {
    if (!req.body.title) {
      return res.status(400).send({
        message: 'NewsArticles name can not be empty',
      });
    }

    if (!req.body.content) {
      return res.status(400).send({
        message: 'NewsArticles content can not be empty',
      });
    }

    const newNewsArticle = new NewsArticles({
      title: req.body.title,
      introduction: req.body.introduction,
      resume: req.body.resume,
      cover: process.env.UPLOAD_DOMAIN + req.file.path,
      content: req.body.content,
      date: new Date(),
    });

    newNewsArticle
      .save()
      .then((data) => {
        res.send(data);
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || 'Some error occurred while creating the NewsArticle.',
        });
      });
  },

  findAll: (req, res) => {
    NewsArticles.find({})
      .exec((err, newsArticles) => {
        if (err) return res.send(err);
        res.send(newsArticles);
      });
  },

  findOne: (req, res) => {
    NewsArticles.findById(req.params.newsArticleId).exec((err, newsArticle) => {
      if (err) return res.send(err);
      res.send(newsArticle);
    });
  },

  update: (req, res) => {
    if (!req.body.title) {
      return res.status(400).send({
        message: 'NewsArticles title can not be empty',
      });
    }

    const handledReqBody = req.body;

    if (req.file) {
      handledReqBody.cover = process.env.UPLOAD_DOMAIN + req.file.path;
    }

    // Find artist and update it with the request body
    NewsArticles.findByIdAndUpdate(req.params.newsArticleId, handledReqBody, {
      new: true,
    })
      .then((newsArticle) => {
        if (!newsArticle) {
          return res.status(404).send({
            message: `NewsArticles not found with id ${req.params.newsArticleId}`,
          });
        }
        res.send(newsArticle);
      })
      .catch((err) => {
        if (err.kind === 'ObjectId') {
          return res.status(404).send({
            message: `NewsArticles not found with id ${req.params.newsArticleId}`,
          });
        }
        return res.status(500).send({
          message: `Error updating newsArticle with id ${req.params.newsArticleId}`,
        });
      });
  },

  delete: (req, res) => {
    NewsArticles.findByIdAndRemove(req.params.newsArticleId)
      .then((newsArticle) => {
        if (!newsArticle) {
          return res.status(404).send({
            message: `NewsArticles not found with id ${req.params.newsArticleId}`,
          });
        }
        res.send({
          message: 'NewsArticles deleted successfully!',
        });
      }).catch((err) => {
        if (err.kind === 'ObjectId' || err.name === 'NotFound') {
          return res.status(404).send({
            message: `NewsArticles not found with id ${req.params.newsArticleId}`,
          });
        }
        return res.status(500).send({
          message: `Could not delete newsArticle with id ${req.params.newsArticleId}`,
        });
      });
  },
};
