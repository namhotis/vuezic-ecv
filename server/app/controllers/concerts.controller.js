const Concert = require('../models/concert.model.js');
const Artist = require('../models/artist.model.js');

module.exports = {
  create: (req, res) => {
    if (!req.body.date) {
      return res.status(400).send({
        message: 'Concert date can not be empty',
      });
    }

    if (!req.body.artist) {
      return res.status(400).send({
        message: 'Concert artist can not be empty',
      });
    }

    const newConcert = new Concert({
      artist: req.body.artist,
      date: req.body.date,
    });

    newConcert.save((error, concert) => {
      if (!error) {
        Artist.findOne({
          _id: concert.artist,
        }).exec((_error, artist) => {
          if (!artist.concerts) {
            artist.concerts = [];
          }

          artist.concerts.push(concert);
          artist.save();
        });

        Concert.findOne({
          _id: concert._id,
        })
          .populate('artist')
          .exec((_error, _concert) => {
            if (error) res.send(_error);
            if (!error && _concert) res.send(_concert);
          });
      }
    });
  },
  artistByConcert: async (req, res) => {
    const {
      artistId,
    } = req.params;
    const artistByConcert = await Concert.findById(artistId).populate('artist');
    res.send(artistByConcert);
  },

  // Return the concerts of the artist
  concertsByArtist: (req, res) => {
    const {
      artistId,
    } = req.params;
    Artist.findById(artistId).populate('concerts')
      .then((concerts) => {
        res.send(concerts);
      })
      .catch((err) => {
        res.status(500).send({
          message: err.message || 'Some error occurred while retrieving concerts.',
        });
      });
  },

  // Retrieve and return all concerts from the database.
  findAll: (req, res) => {
    Concert.find({})
      .populate('artist')
      .exec((err, concerts) => {
        if (err) return res.send(err);
        res.send(
          concerts.map((concert) => {
            const tmpConcert = {
              _id: concert._id,
              artist: null,
              date: concert.date,
            };

            if (concert.artist) {
              tmpConcert.artist = {
                name: concert.artist.name,
                profile_picture: concert.artist.profile_picture,
                _id: concert.artist._id,
              };
            }

            return tmpConcert;
          }),
        );
      });
  },

  // Find a single concert with a concertId
  findOne: (req, res) => {
    Concert.findById(req.params.concertId)
      .then((concert) => {
        if (!concert) {
          return res.status(404).send({
            message: `Concert not found with id ${req.params.concertId}`,
          });
        }

        concert.artist = {
          date: concert.artist.date,
          artist: req.body.artistId,
        };

        res.send(concert);
      }).catch((err) => {
        if (err.kind === 'ObjectId') {
          return res.status(404).send({
            message: `Concert not found with id ${req.params.concertId}`,
          });
        }
        return res.status(500).send({
          message: `Error retrieving concert with id ${req.params.concertId}`,
        });
      });
  },

  // Update a concert identified by the concertId in the request
  update: (req, res) => {
    // Validate Request
    if (!req.body.date) {
      return res.status(400).send({
        message: 'Concert date can not be empty',
      });
    }

    // Find concert and update it with the request body
    Concert.findByIdAndUpdate(
      req.params.concertId, req.body, {
        new: true,
      },
    )
      .then((concert) => {
        if (!concert) {
          return res.status(404).send({
            message: `Concert not found with id ${req.params.concertId}`,
          });
        }
        res.send(concert);
      })
      .catch((err) => {
        if (err.kind === 'ObjectId') {
          return res.status(404).send({
            message: `Concert not found with id ${req.params.concertId}`,
          });
        }
        return res.status(500).send({
          message: `Error updating concert with id ${req.params.concertId}`,
        });
      });
  },

  // Delete a concert with the specified concertId in the request
  delete: (req, res) => {
    Concert.findByIdAndRemove(req.params.concertId)
      .then((concert) => {
        if (!concert) {
          return res.status(404).send({
            message: `Concert not found with id ${req.params.concertId}`,
          });
        }
        res.send({
          message: 'Concert deleted successfully!',
        });
      }).catch((err) => {
        if (err.kind === 'ObjectId' || err.name === 'NotFound') {
          return res.status(404).send({
            message: `Concert not found with id ${req.params.concertId}`,
          });
        }
        return res.status(500).send({
          message: `Could not delete concert with id ${req.params.concertId}`,
        });
      });
  },
};
