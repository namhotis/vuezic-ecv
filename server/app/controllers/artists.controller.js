const Artist = require('../models/artist.model.js');

module.exports = {
  create: (req, res) => {
    if (!req.body.name) {
      return res.status(400).send({
        message: 'Artist name can not be empty',
      });
    }

    if (!req.body.description) {
      return res.status(400).send({
        message: 'Artist description can not be empty',
      });
    }

    const newArtist = new Artist({
      name: req.body.name,
      description: req.body.description,
      profile_picture: process.env.UPLOAD_DOMAIN + req.file.path,
    });

    newArtist
      .save()
      .then((data) => {
        res.status(200).send(data);
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || 'Some error occurred while creating the Artist.',
        });
      });
  },

  // Retrieve and return all artists from the database.
  findAll: (req, res) => {
    Artist.find({})
      .populate('albums')
      .exec((err, artists) => {
        if (err) return res.send(err);
        res.send(artists);
      });
  },

  // Find a single artist with a artistId
  findOne: (req, res) => {
    Artist.findById(req.params.artistId)
      .populate('albums')
      .exec((err, artists) => {
        if (err) return res.send(err);
        res.send(artists);
      });
  },

  // Update a artist identified by the artistId in the request
  update: (req, res) => {
    // Validate Request
    if (!req.body.name) {
      return res.status(400).send({
        message: 'Artist name can not be empty',
      });
    }

    const handledReqBody = req.body;

    if (req.file) {
      handledReqBody.profile_picture = process.env.UPLOAD_DOMAIN + req.file.path;
    }

    // Find artist and update it with the request body
    // On envoi req.body pour ne pas modifier les autres champs !
    Artist.findByIdAndUpdate(req.params.artistId, handledReqBody, {
      // Retourne la data après que la modification ait été effectuée
      new: true,
    })
      .then((artist) => {
        if (!artist) {
          return res.status(404).send({
            message: `Artist not found with id ${req.params.artistId}`,
          });
        }
        res.send(artist);
      })
      .catch((err) => {
        if (err.kind === 'ObjectId') {
          return res.status(404).send({
            message: `Artist not found with id ${req.params.artistId}`,
          });
        }
        return res.status(500).send({
          message: `Error updating artist with id ${req.params.artistId}`,
        });
      });
  },

  // Delete a artist with the specified artistId in the request
  delete: (req, res) => {
    Artist.findByIdAndRemove(req.params.artistId)
      .then((artist) => {
        if (!artist) {
          return res.status(404).send({
            message: `Artist not found with id ${req.params.artistId}`,
          });
        }
        res.send({
          message: 'Artist deleted successfully!',
        });
      }).catch((err) => {
        if (err.kind === 'ObjectId' || err.name === 'NotFound') {
          return res.status(404).send({
            message: `Artist not found with id ${req.params.artistId}`,
          });
        }
        return res.status(500).send({
          message: `Could not delete artist with id ${req.params.artistId}`,
        });
      });
  },
};
