const mongoose = require('mongoose');

const ConcertSchema = mongoose.Schema(
  {
    date: String,
    artist: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Artist',
    },
  },
  {
    versionKey: false,
  },
);

module.exports = mongoose.model('Concert', ConcertSchema);
