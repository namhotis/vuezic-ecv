const mongoose = require('mongoose');

const AlbumSchema = mongoose.Schema(
  {
    name: String,
    release_date: Date,
    cover: String,
    artist: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Artist',
    },
  },
  {
    versionKey: false,
  },
);

module.exports = mongoose.model('Album', AlbumSchema);
