const mongoose = require('mongoose');

const NewsArticleSchema = mongoose.Schema(
  {
    title: String,
    introduction: String,
    resume: String,
    cover: String,
    date: Date,
    content: String,
  },
  {
    versionKey: false,
  },
);

module.exports = mongoose.model('NewsArticle', NewsArticleSchema);
