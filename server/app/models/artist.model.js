const mongoose = require('mongoose');

const ArtistSchema = mongoose.Schema(
  {
    name: String,
    description: String,
    profile_picture: String,
    albums: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Album',
    }],
  }, {
    versionKey: false,
  },
);

module.exports = mongoose.model('Artist', ArtistSchema);
