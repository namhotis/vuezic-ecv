/* eslint-disable no-console */
const colors = require('colors');
const express = require('express');
const bodyParser = require('body-parser');
const terminalLink = require('terminal-link');
const mongoose = require('mongoose');
const passport = require('passport');
const cors = require('cors');
const dbConfig = require('./app/config/database.config');

require('dotenv').config();
require('./app/auth/auth');
const secureRoute = require('./app/routes/secure-routes');

const app = express();

app.use(cors());
app.use('/uploads', express.static(`${__dirname}/uploads`));

app.use(
  bodyParser.urlencoded({
    extended: true,
  }),
);
app.use(bodyParser.json());

mongoose.set('useCreateIndex', true);

mongoose
  .connect(dbConfig.url, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
  })
  .then(() => {
    console.log('Successfully connected to the DB');
  })
  .catch((err) => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
  });

require('./app/routes/artists.routes.js')(app);
require('./app/routes/albums.routes.js')(app);
require('./app/routes/concerts.routes.js')(app);
require('./app/routes/newsArticles.routes.js')(app);
require('./app/routes/users.routes.js')(app);

app.use('/user', passport.authenticate('jwt', { session: false }), secureRoute);

// Handle errors.
app.use((err, req, res, next) => {
  res.status(err.status || 500);
  res.json({ error: err });
});

app.listen(process.env.PORT, () => {
  const coloredPort = colors.red.blue(
    terminalLink(process.env.PORT, `http://localhost:${process.env.PORT}`),
  );

  const text = `Server is listening on port ${coloredPort}.`;
  console.log(text);
});
