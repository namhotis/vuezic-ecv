import fs from 'fs';

const multer = require('multer');

const storage = multer.diskStorage({
  destination(req, file, cb) {
    const path = './uploads/';
    fs.mkdirSync(path, { recursive: true });
    cb(null, path);
  },
  filename(req, file, cb) {
    const uniqueSuffix = `${Date.now()}-${Math.round(Math.random() * 1e9)}`;
    cb(null, `${file.fieldname}-${uniqueSuffix}`);
  },
});

const uploadDisk = multer({
  storage,
});

export default uploadDisk;
